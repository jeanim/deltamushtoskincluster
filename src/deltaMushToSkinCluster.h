//
//  deltaMushToSkinCluster.h
//  deltaMushToSkinCluster
//
//  Created by Godfrey Huang on 10/9/15.
//
//

#include <maya/MArgList.h>
#include <maya/MSyntax.h>
#include <maya/MString.h>
#include <maya/MDagPath.h>
#include <maya/MBoundingBox.h>
#include <maya/MPxCommand.h>

#ifndef deltaMushToSkinCluster_h
#define deltaMushToSkinCluster_h

class deltaMushToSkinCluster : public MPxCommand
{
public:
                    deltaMushToSkinCluster();
    virtual         ~deltaMushToSkinCluster();
    
    MStatus         doIt( const MArgList& args );
    
    static MSyntax  newSyntax();
    static void*    creator();
private:
    MStatus         parseArgs( const MArgList& args,
                              MString& source,
                              MString& target);
    MObject         findSkinCluster(MDagPath&);
    MBoundingBox    getBoundingBox(MDagPath&);
    
    MString         source;
    MString         target;
};

#endif /* deltaMushToSkinCluster_h */
